#!/usr/bin/env node
'use strict';

import { ArgumentParser } from 'argparse'
import * as request from 'request-promise-native'
import * as turf from '@turf/turf'
import * as wellknown from 'wellknown'
import { promises as fs } from 'fs'
import { Polygon } from '@turf/turf';
import { RPCModel } from './rpc'
import { BBox } from '@turf/helpers/lib/geojson';
import * as egm96 from 'earthgravitymodel1996'
import { range } from 'lodash'
import * as Handlebars from 'handlebars'
import * as path from 'path'
import * as geotiff from 'geotiff'

Handlebars.registerHelper("math", function(lvalue:string, operator:string, rvalue:string, options) {
    const left = parseFloat(lvalue);
    const right = parseFloat(rvalue);

    switch (operator) {
        case "+": return left + right;
        case "-": return left - right;
        case "*": return left * right;
        case "%": return left / right;
        default: return null;
    };
});

var parser = new ArgumentParser({
    version: '0.0.1',
    addHelp: true,
    description: 'Tool to obtian Level1B imagery from GBDX'
});

parser.addArgument(
    ['catalog_id'],
    {
        help: 'A DG catalog id'
    }
);

parser.addArgument(
    ['geometry'],
    {
        help: 'Geometry to download image for'
    }
);

parser.addArgument(
    ['--type'],
    {
        help: "Pancromatic or multispectral. ['pan', 'mul'].  Default 'pan'",
        choices: ['pan', 'mul'],
        defaultValue: 'pan',
    }
);

parser.addArgument(
    ['--dem'],
    {
        help: "Optional DEM (relative to EGM96 geoid) to use when spatial coords to tiles",
    }
);

parser.addArgument(
    ['--tile-size'],
    {
        help: 'Chip tile size to use during download',
        type: 'int',
        defaultValue: 32,
    }
);

parser.addArgument(
    ['--clobber'],
    {
        help: 'Whether to overwrite existing chips if they already exists.',
        action: 'storeTrue',
    }
);


const commaSeparatedIntList = (s: string): number[] => {
    return s.split(',').map(i => parseInt(i))
}

parser.addArgument(
    ['--bands'],
    {
        help: 'Comma separated list of band indices to download (zero indexed)',
        type: commaSeparatedIntList,
    }
);

parser.addArgument(
    ['-o', '--output'],
    {
        help: 'Output directory.',
    }
);


var args = parser.parseArgs();

const USERNAME = 'Justine.belcher@Geospatial-insight.com'
const PASSWORD = 'Gsinsight06'

type Coord2d = [number, number]

// TODO: make these interfaces [id: string]: any; so we are forced to sanity check the 3rd party input.
interface ITileMetadata {
    rpcSensorModel: any
    imageMetadata: any
}

interface IStripTileMetadata {
    imageBoundsWGS84: string
    imageId: string
    tileBucketName: string
    numBands: number
}

interface IStripMetadata {
    catalogIdentifier: string
    vnirImages: IStripTileMetadata[]
    panImages: IStripTileMetadata[]
}

type ImageType = 'pan' | 'mul'

class RDA {

    private static GRAPH_AND_NODE_BY_IMAGE_TYPE: Map<ImageType, [string, string]> = new Map([
        ['mul', ['2f640d0fd149c9cefa85363f7415858c74412ff15072739071146a5ce1823bf7','TileSize']],
        ['pan', ['2f640d0fd149c9cefa85363f7415858c74412ff15072739071146a5ce1823bf7','TileSize']],
    ])

    private token: string | null = null

    public constructor(
        private readonly username: string,
        private readonly password: string,
        private readonly tileSize: number,
    ) {

    }

    private async getToken(): Promise<string> {
        const tokenResponse = await request.post(`https://geobigdata.io/auth/v1/oauth/token/`, {
            json: true,
            body: {
                grant_type: 'password',
                username: this.username,
                password: this.password,
            }
        })
        return tokenResponse.access_token
    }

    private async get(url: string, parseJson: boolean = false): Promise<any> {
        console.log(url)
        this.token = this.token || await this.getToken()
        try {
            const response = await request(url, {
                json: parseJson,
                encoding: null,
                headers: {
                    authorization: `Bearer ${this.token || ""}`
                }
            })
            return response
        } catch (e) {
            if (e.statusCode === 401) {
                this.token = await this.getToken()
                return this.get(url, parseJson)
            } else {
                throw e
            }
        }
    }

    async getStripMetadata(catalogId: string): Promise<IStripMetadata> {
        return this.get(`https://rda.geobigdata.io/v1/stripMetadata/${catalogId}`, true)
    }

    async getTileMetadataFromRDAGraph(imageType: 'pan' | 'mul', imageId: string, bucketName: string, bands: number[]): Promise<ITileMetadata> {
        const [graph, node] = RDA.GRAPH_AND_NODE_BY_IMAGE_TYPE.get(imageType) || [undefined, undefined]
        if (!graph || !node) {
            throw new Error(`Unknown graph or node for image type ${imageType}`)
        }
        return this.get(`https://rda.geobigdata.io/v1/template/${graph}/metadata?nodeId=${node}&imageId=${imageId}&bucketName=${bucketName}&tileSize=${this.tileSize}&bands=${bands.join(',')}`, true)
    }

    async getChip(imageType: 'pan' | 'mul', imageId: string, bucketName: string, tileIndex: [number, number], bands: number[]): Promise<Buffer> {
        const [graph, node] = RDA.GRAPH_AND_NODE_BY_IMAGE_TYPE.get(imageType) || [undefined, undefined]
        if (!graph || !node) {
            throw new Error(`Unknown graph or node for image type ${imageType}`)
        }
        return this.get(`https://rda.geobigdata.io/v1/template/${graph}/tile/${tileIndex[0]}/${tileIndex[1]}?nodeId=${node}&imageId=${imageId}&bucketName=${bucketName}&tileSize=${this.tileSize}&bands=${bands.join(',')}`)
    }

    async getRPC(imageId: string, bucketName: string, metadataType: 'xml' | 'rpb' = 'xml'): Promise<Buffer> {
        return this.get(`https://idaho.geobigdata.io/v1/vendor-metadata/${bucketName}/${imageId}/${metadataType.toUpperCase()}.metadata`)
    }
}

async function getEllipsoidalHeight(lon: number, lat: number, dem?: string): Promise<number> {

    const geoidHeight = egm96.getHeight(lon * Math.PI / 180.0, lat * Math.PI / 180.0)
    console.log("Geoid height = ", geoidHeight)

    if (!dem) return geoidHeight

    const tiff = await geotiff.fromFile(dem)
    const img = await tiff.getImage()
    const res = img.getResolution()
    const orig = img.getOrigin()
    const x = (lon-orig[0])/res[0]
    const y = (lat-orig[1])/res[1]
    // TODO: Fix the window in case x and y are integer values.
    const window = [
        Math.floor(x),
        Math.floor(y),
        Math.ceil(x),
        Math.ceil(y)
    ]
    const demHeights = await img.readRasters({window, interleave: true});
    const mean = (a: number[]) => a.reduce((acc, x) => acc + x) / a.length
    const demHeight = mean(demHeights)
    console.log("DEM height = ", demHeight)
    return geoidHeight + demHeight
}

function createOutputFilepath(filename: string, outputDir?: string): string {
    let r = filename;
    if (!!outputDir) {
        r = path.join(outputDir, filename);
    }
    return r;
}

async function main() {
    const geojson = JSON.parse(await fs.readFile(args.geometry, 'utf-8'))
    const convexHull = turf.convex(geojson)

    const rda = new RDA(USERNAME, PASSWORD, args.tile_size)
    const stripMetadata = await rda.getStripMetadata(args.catalog_id)
    const imageParts = args.type === 'pan' ? stripMetadata.panImages : stripMetadata.vnirImages

    const tile = imageParts
        .find(t => {
            const tileBounds = wellknown.parse(t.imageBoundsWGS84) as Polygon
            return turf.booleanContains(tileBounds, convexHull)
        })

    if (!tile) {
        throw new Error(`Geometry not completely contained in any one pan-tile for ${args.catalog_id}`)
    }

    console.log(tile)

    const bands = [...Array(tile.numBands).keys()]
        .map(x => x)
        .filter(b => !args.bands || args.bands.includes(b))

    const tileMetadata = await rda.getTileMetadataFromRDAGraph(args.type, tile.imageId, tile.tileBucketName, bands)

    console.log(tileMetadata)

    const rpc = RPCModel.fromDGMetadata(tileMetadata.rpcSensorModel)

    const bboxToCornerCoords = (bbox: BBox): Coord2d[] => [[bbox[0], bbox[3]], [bbox[2], bbox[3]], [bbox[2], bbox[1]], [bbox[0], bbox[1]]]
    const coordsTobbox = (coords: Coord2d[]): BBox => [
        Math.min(...coords.map(c => c[0])),
        Math.min(...coords.map(c => c[1])),
        Math.max(...coords.map(c => c[0])),
        Math.max(...coords.map(c => c[1]))
    ]

    const spatialCornerCoords = bboxToCornerCoords(turf.bbox(convexHull))

    const imageCornerCoords = Promise.all(spatialCornerCoords.map(async x => rpc.apply(x[0], x[1], await getEllipsoidalHeight(x[0], x[1], args.dem))))
    const imageBbox = coordsTobbox(await imageCornerCoords)

    const chipIndices = [
        Math.floor(imageBbox[0] / tileMetadata.imageMetadata.tileXSize),
        Math.floor(imageBbox[1] / tileMetadata.imageMetadata.tileYSize),
        Math.floor(imageBbox[2] / tileMetadata.imageMetadata.tileXSize),
        Math.floor(imageBbox[3] / tileMetadata.imageMetadata.tileYSize)
    ]

    const chips =
        range(chipIndices[0], chipIndices[2] + 1).flatMap((i, iidx) =>
            range(chipIndices[1], chipIndices[3] + 1).flatMap((j, jidx) => ({
                filename: `${args.catalog_id}_${args.type}_${i}_${j}.tif`,
                xindex: i,
                yindex: j,
                xsize: tileMetadata.imageMetadata.tileXSize,
                ysize: tileMetadata.imageMetadata.tileYSize,
                xoffset: i * tileMetadata.imageMetadata.tileXSize,
                yoffset: j * tileMetadata.imageMetadata.tileYSize
            }))
        )

    console.log(chips)

    if (!args.output)
        return

    const downloadChip = async (filename: string, chipIndex: [number, number], retries: number = 0): Promise<void> => {
        const fullFilename = createOutputFilepath(filename, args.output);
        let exists = false;
        try {
            await fs.stat(fullFilename);
            exists = true;
        } catch (e) {
        }
        if (!exists || !!args.clobber) {
            try {
                const chipTif = await rda.getChip(args.type, tile.imageId, tile.tileBucketName, chipIndex, bands)
                await fs.writeFile(fullFilename, chipTif)
            } catch (e) {
                console.error(e)
                if (retries < 10) {
                    await downloadChip(filename, chipIndex, retries+1)
                } else {
                    throw e
                }
            }
        }
    }

    await Promise.all(chips.map(chip => downloadChip(chip.filename, [chip.xindex, chip.yindex])))

    const createVrt = Handlebars.compile(await fs.readFile('vrtTemplate.handlebars', 'utf-8'))
    const vrt = createVrt({
        rasterXSize: tileMetadata.imageMetadata.imageWidth,
        rasterYSize: tileMetadata.imageMetadata.imageHeight,
        chips: chips,
        bands: [...Array(bands.length).keys()].map(x => x+1),
        rpc: rpc,
    })


    const mergedTifBasename = [
        args.catalog_id,
        args.type,
        Math.floor(imageBbox[0]),
        Math.floor(imageBbox[1]),
        Math.ceil(imageBbox[2]-imageBbox[0]),
        Math.ceil(imageBbox[3]-imageBbox[1]),
        tileMetadata.imageMetadata.groundSampleDistanceMeters,
    ].join('_')

    const [vrtFilename, rpcXmlFilename, rpcRpbFilename] = ['vrt', 'xml', 'rpb']
        .map(e => createOutputFilepath(`${mergedTifBasename}.${e}`, args.output));

    await fs.writeFile(vrtFilename, vrt)
    const rpcXml = await rda.getRPC(tile.imageId, tile.tileBucketName, 'xml')
    await fs.writeFile(rpcXmlFilename, rpcXml, {encoding: 'utf-8'})
    const rpcRpb = await rda.getRPC(tile.imageId, tile.tileBucketName, 'rpb')
    await fs.writeFile(rpcRpbFilename, rpcRpb, {encoding: 'utf-8'})
    console.log(vrtFilename, rpcXmlFilename)
}

main()
