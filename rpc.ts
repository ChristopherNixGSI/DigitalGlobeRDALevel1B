import * as pegjs from 'pegjs'
import * as fs from 'fs'
import * as path from 'path'

class RPCModel {
  /**
   * An RPC model to support working with Level1B satellite data.
   *
   * We'll use this class to work out pixel bounds within a Level1B image that
   * correspond to a lon-lat AOI.
   */

  public constructor (
    public readonly xScaleAndOffset: ScaleAndOffset,
    public readonly yScaleAndOffset: ScaleAndOffset,
    public readonly lonScaleAndOffset: ScaleAndOffset,
    public readonly latScaleAndOffset: ScaleAndOffset,
    public readonly heightScaleAndOffset: ScaleAndOffset,
    public readonly xRFM: RationalFunctionalModel,
    public readonly yRFM: RationalFunctionalModel
  ) {

  }

  static fromRPB (rpb: string): RPCModel {
    return RPCModel.RPB_PARSER.parse(rpb, { RPCModelBuilder })
  }

  static fromDGMetadata (rpcSensorModel: {[id: string]: any;}): RPCModel {
    const rpcModelBuilder = new RPCModelBuilder()
    rpcModelBuilder.xScale = rpcSensorModel.sampleScale
    rpcModelBuilder.xOffset = rpcSensorModel.sampleOffset
    rpcModelBuilder.yScale = rpcSensorModel.lineScale
    rpcModelBuilder.yOffset = rpcSensorModel.lineOffset
    rpcModelBuilder.lonScale = rpcSensorModel.lonScale
    rpcModelBuilder.lonOffset = rpcSensorModel.lonOffset
    rpcModelBuilder.latScale = rpcSensorModel.latScale
    rpcModelBuilder.latOffset = rpcSensorModel.latOffset
    rpcModelBuilder.heightScale = rpcSensorModel.heightScale
    rpcModelBuilder.heightOffset = rpcSensorModel.heightOffset
    rpcModelBuilder.xRFMNumerator = rpcSensorModel.sampleNumCoefs
    rpcModelBuilder.xRFMDenominator = rpcSensorModel.sampleDenCoefs
    rpcModelBuilder.yRFMNumerator = rpcSensorModel.lineNumCoefs
    rpcModelBuilder.yRFMDenominator = rpcSensorModel.lineDenCoefs
    return rpcModelBuilder.build()
  }

  public apply (lon: number, lat: number, height: number): [number, number] {
    const normalize = (x: number, { scale, offset }: ScaleAndOffset) => (x - offset) / scale
    const denormalize = (x: number, { scale, offset }: ScaleAndOffset) => (x * scale) + offset

    const uLon = normalize(lon, this.lonScaleAndOffset)
    const uLat = normalize(lat, this.latScaleAndOffset)
    const uHeight = normalize(height, this.heightScaleAndOffset)

    const uX = this.applyRFM(this.xRFM, uLon, uLat, uHeight)
    const uY = this.applyRFM(this.yRFM, uLon, uLat, uHeight)

    const x = denormalize(uX, this.xScaleAndOffset)
    const y = denormalize(uY, this.yScaleAndOffset)

    return [x, y]
  }

  private applyPolynomial (poly: Polynomial, x: number, y: number, z: number): number {
    let r = 0
    r += poly[0]
    r += poly[1] * y + poly[2] * x + poly[3] * z
    r += poly[4] * y * x + poly[5] * y * z + poly[6] * x * z
    r += poly[7] * y * y + poly[8] * x * x + poly[9] * z * z
    r += poly[10] * x * y * z
    r += poly[11] * y * y * y
    r += poly[12] * y * x * x + poly[13] * y * z * z + poly[14] * y * y * x
    r += poly[15] * x * x * x
    r += poly[16] * x * z * z + poly[17] * y * y * z + poly[18] * x * x * z
    r += poly[19] * z * z * z
    return r
  }

  private applyRFM (rfm: RationalFunctionalModel, uLon: number, uLat: number, uHeight: number): number {
    // NB. lon, lat change parameter order here so that we closely match
    // the RPB spec of how to apply the polynomials in 3 dimensions.
    const n = this.applyPolynomial(rfm.numerator, uLat, uLon, uHeight)
    const d = this.applyPolynomial(rfm.denominator, uLat, uLon, uHeight)
    return n / d
  }

  private static readonly RPB_PARSER = pegjs.generate(
    fs.readFileSync(path.join(__dirname, 'rpb.pegjs'), 'utf-8')
  )

}

interface ScaleAndOffset {
  readonly scale: number
  readonly offset: number
}

// Polynomial of 20 coefficients
type Polynomial = readonly [number, number, number, number, number, number, number, number, number, number, number, number, number, number, number, number, number, number, number, number]

interface RationalFunctionalModel {
  readonly numerator: Polynomial
  readonly denominator: Polynomial
}

class RPCModelParseError extends Error { }

class RPCModelBuilder {
  /**
   * A builder to do the accumulation of fields whilst parsing an RPB file.
   */
  public xScale?: number
  public xOffset?: number
  public yScale?: number
  public yOffset?: number
  public lonScale?: number
  public lonOffset?: number
  public latScale?: number
  public latOffset?: number
  public heightScale?: number
  public heightOffset?: number
  public xRFMNumerator?: Polynomial
  public xRFMDenominator?: Polynomial
  public yRFMNumerator?: Polynomial
  public yRFMDenominator?: Polynomial

  public build (): RPCModel {
    /**
     * Build an RPCModel with some helpful sanity checks.
     *
     * The checks are required because we are invoking this from outside
     * of typescript (eg from the pegjs parser from an RPB file) and so the compiler
     * doesn't get a chance to check the params.  We could put these checks in the
     * RPCModel constructor, but it feels nice to keep that as vanilla typescript and
     * instead do the type-checks here as the interface from the pegjs parser.
     */
    const missing = (p: string) => new RPCModelParseError(`Missing ${p} parameter in RPC model`)
    const short = (p: string) => new RPCModelParseError(`Insufficient elements in ${p} polynomial`)

    if (this.xScale == null ) throw missing('x scale')
    if (this.xOffset == null) throw missing('x offset')
    if (this.yScale == null) throw missing('y scale')
    if (this.yOffset == null) throw missing('y offset')
    if (this.lonScale == null) throw missing('lon scale')
    if (this.lonOffset == null) throw missing('lon offset')
    if (this.latScale == null) throw missing('lat scale')
    if (this.latOffset == null) throw missing('lat offset')
    if (this.heightScale == null) throw missing('height scale')
    if (this.heightOffset == null) throw missing('height offset')
    if (this.xRFMNumerator == null) throw missing('x numerator')
    if (this.xRFMDenominator == null) throw missing('x denominator')
    if (this.yRFMNumerator == null) throw missing('y numerator')
    if (this.yRFMDenominator == null) throw missing('y denominator')
    if (this.xRFMNumerator.length < 20) short('x numerator')
    if (this.xRFMDenominator.length < 20) throw missing('x denominator')
    if (this.yRFMNumerator.length < 20) throw missing('y numerator')
    if (this.yRFMDenominator.length < 20) throw missing('y denominator')

    return new RPCModel(
      { scale: this.xScale, offset: this.xOffset },
      { scale: this.yScale, offset: this.yOffset },
      { scale: this.lonScale, offset: this.lonOffset },
      { scale: this.latScale, offset: this.latOffset },
      { scale: this.heightScale, offset: this.heightOffset },
      { numerator: this.xRFMNumerator, denominator: this.xRFMDenominator },
      { numerator: this.yRFMNumerator, denominator: this.yRFMDenominator }
    )
  }

}

export { RPCModel, RPCModelParseError }
