// Parsing of RPC00B files, otherwise known as RPB files.

{
    const builder = new options.RPCModelBuilder();
}

RPB = Statements 'END;' _                           { return builder.build() }

Statements = (_ Statement _)+

Statement
  =  KeyAndValue _ ';'
  / 'BEGIN_GROUP = IMAGE' _ Statements _ 'END_GROUP = IMAGE'

KeyAndValue
  = 'sampScale'    _ '=' _ v:Integer                { builder.xScale = v }
  / 'sampOffset'   _ '=' _ v:Integer                { builder.xOffset = v }
  / 'lineScale'    _ '=' _ v:Integer                { builder.yScale = v }
  / 'lineOffset'   _ '=' _ v:Integer                { builder.yOffset = v }
  / 'longScale'    _ '=' _ v:Float                  { builder.lonScale = v }
  / 'longOffset'   _ '=' _ v:Float                  { builder.lonOffset = v }
  / 'latScale'     _ '=' _ v:Float                  { builder.latScale = v }
  / 'latOffset'    _ '=' _ v:Float                  { builder.latOffset = v }
  / 'heightScale'  _ '=' _ v:Float                  { builder.heightScale = v }
  / 'heightOffset' _ '=' _ v:Float                  { builder.heightOffset = v }
  / 'sampNumCoef'  _ '=' _ '(' v:Polynomial ')'     { builder.xRFMNumerator = v }
  / 'sampDenCoef'  _ '=' _ '(' v:Polynomial ')'     { builder.xRFMDenominator = v }
  / 'lineNumCoef'  _ '=' _ '(' v:Polynomial ')'     { builder.yRFMNumerator = v }
  / 'lineDenCoef'  _ '=' _ '(' v:Polynomial ')'     { builder.yRFMDenominator = v }
  / [a-zA-Z]+ _ '=' _ (! ';' .)+

Polynomial = _ head:Float _ tail:(',' _ Float _)*   { return tail.reduce((acc, x) => acc.concat(x[2]), [head]) }

Float
  = ('-' / '+')? Integer ('.' Integer)? (('E' / 'e') ('-' / '+')? Integer)? { return parseFloat(text()) }

Integer = [0-9]+                                    { return parseInt(text()) }

_ "whitespace" = [ \t\n\r]*
